#include <iostream>
#include "superSt.h"

using namespace std;

int main() {
	int a = 0;

 	SuperString st1("hola");
        SuperString st2("mundo");
        st1 = st1 + st2;
        cout << st1 << endl;

	SuperString s2 = static_cast<string>("3 4 5 + * 1 +");
	if ( s2.evalPostfix(a)) {
		cout << "a: " << a << endl;
	}

	return 0;
}
